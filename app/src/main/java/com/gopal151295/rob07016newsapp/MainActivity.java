package com.gopal151295.rob07016newsapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    public static final String ARTICLE_URL = "ARTICLE_URL";
    private ListView listView;
    private List<String> newsTitlesList;
    private List<String>newsUrlsList;
    private String[] newsTitles;
    private String[] newsUrls;
    private ArrayAdapter<String> adapter;
    private int max = 20;
    private View progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.activity_main_list_news);
        progress = findViewById(R.id.progressFrame);
//        newsTitlesList = new ArrayList<>();

        newsTitles= new String[max];
        newsUrls = new String[max];

        String bestStroriesJsonArrayId = "";

        DownloadData bestStoriesIds = new DownloadData();
        try {
            bestStroriesJsonArrayId = bestStoriesIds.execute("https://hacker-news.firebaseio.com/v0/beststories.json?print=pretty").get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        String[] articleId = {};

        if (bestStroriesJsonArrayId != null && !bestStroriesJsonArrayId.isEmpty()){
            Log.i(TAG, "onCreate: matching");
//            Pattern p = Pattern.compile("(?<=\\[ )[^]]+(?= \\])");
            Pattern p = Pattern.compile("(?<=\\[ ).+?(?= \\])");
            Matcher m = p.matcher(bestStroriesJsonArrayId);
            if (m.find()){
                articleId = m.group(0).split(", ");
            }
            for (String id: articleId){
                Log.i(TAG, "onCreate: " + id);
            }
        }

        Log.i(TAG, "onCreate: " + articleId.length);

        String[] bestStoriesJsons =new String[max];
        for (int i = 0; i < max; i++) {
            DownloadData bestStoriesJsonTask = new DownloadData();
            try {
                String urlString = "https://hacker-news.firebaseio.com/v0/item/" + articleId[i] + ".json?print=pretty";
                bestStoriesJsons[i] = bestStoriesJsonTask.execute(urlString).get();
//                Log.i(TAG, "onCreate: inside loop"+ bestStoriesJsons[i]);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }




//        for (String bestStoriesJson : bestStoriesJsons) {
////            Log.i(TAG, "onCreate: bestStoriesJson" + bestStoriesJson);
//            try {
//                JSONObject jsonObject = new JSONObject(bestStoriesJson);
//                new
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//
//        }

        for (int i = 0; i < max ; i++) {
            try {
                JSONObject jsonObject = new JSONObject(bestStoriesJsons[i]);
                newsTitles[i] = jsonObject.getString("title");
                newsUrls[i] = jsonObject.getString("url");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        newsTitlesList = new ArrayList<>(Arrays.asList(newsTitles));
        newsUrlsList = new ArrayList<>(Arrays.asList(newsUrls));

        adapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_list_item_1, newsTitlesList);
        listView.setAdapter(adapter);

        progress.setVisibility(View.GONE);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), NewsActivity.class);
                intent.putExtra(ARTICLE_URL, newsUrls[position]);
                startActivity(intent);
            }
        });

    }

    public class DownloadData extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                InputStream in = connection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);

                int data = reader.read();
                String result = "";
                while (data != -1){
                    char current = (char) data;
                    result += current;

                    data = reader.read();
                }

                return result;
            }  catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }
    }
}
